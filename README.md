# RChip8

RChip8 is an implementation of the CHIP-8 Virtual Machine written in the Rust programming language

## Prerequisites
The only prerequisite that needs to be manually installed is the Rust toolchain. Other dependencies will be pulled in by Cargo.
Follow these instructions to install Rust: https://www.rust-lang.org/en-US/.

It has been tested on Linux and Windows, and in theory should build/run on Mac OSX as well.

## Compilation
```bash
$ git clone https://gitlab.com/avkhanov/rchip8.git
$ cd rchip8
$ cargo build
```

## Execution
To run using cargo:
```bash
$ cargo run --bin rchip8 <ROM_FILE>
```
To run directly:
```bash
$ cd target/debug/native
$ ./rchip8 <ROM_FILE>
```

A bunch of community-made, freely distributable ROMs are included in the roms/ directory

# RChip8-ASM

(Assembly documentation is still under construction)

RChip8 comes with its own Assembler, so you can write & compile your own CHIP-8 ROMs. Core functionality is complete, but I want to improve error feedback to make it more clear what is wrong in the input file.

## Compiling your code

Through Cargo:
```bash
$ cargo run --bin rchip8-asm <ASM-SOURCE-FILE> <OUTPUT-ROM-NAME>
```

or,


```bash
$ cd target/debug/native
$ ./rchip8-asm <ASM-SOURCE-FILE> <OUTPUT-ROM-NAME>
```

## Comments

Comments are denoted with a pound symbol ('#')

```
# This is a comment!
label: ld $v0, 0
       ld $v1, 0  # This is also a comment!
```
       

## Numbers

Due to limitations of the CHIP-8 architecture, all numbers must fit in one byte. Decimal, hex, and binary notations are supported:

```
45        # Decimal
0x2D      # Hexadecimal
0b101101  # Binary
```

## Labels

Labels are defined like in most other assemblers:

```
label_name: ...
            ...
```
Label names can contain alphanumeric characters and underscores; The first character must be a letter ([a-zA-Z]) or an underscore.

## Syntax

The syntax of each line is very simple:

1. Each line can optionally start with a label
2. One statement per line
3. A statement is either an operation (which will be compiled into a 16-bit opcode) or a byte value, which will be written directly to the ROM. This is used to define program data (such as sprites)

Example:
```
start:      cls
            ld $v0, 0
            ld $v1, 0
            ld $i, my_sprite
            drw $v0, $v1, 8
loop:       jp loop

my_sprite:  0b11111111
            0b10000001
            0b10111101
            0b10100101
            0b10100101
            0b10111101
            0b10000001
            0b11111111
```

Compiling and running the above would draw two rectangles, one inside the other.


## Operations

| Syntax                  | Explanation |
|-------------------------|-------------|
|CLS                      | Clear screen                        
|CALL <label>             | Call subprocedure at <label>
|RET                      | Return from subprocedure to address after the original CALL
|JP <label>               | Jump to label
|JP $vX, <label>          | Jump to address <label> + value of register $vX
|SE $vX, <byte>           | Skip next instruction if value at $vX == <byte>
|SE $vX, $vY              | Skip next instruction if value at $vX == value at $vY
|SNE $vX, <byte>          | Skip next instruction if value at $vX != <byte>
|SNE $vX, $vY             | Skip next instruction if value at $vX != value at $vY
|DRW $vX, $vY, <nibble>   | Draw sprite (stored in register $I) on screen at location ($vX, $vY), with height <nibble>
|LD $vX, <byte>           | Load <byte> into register $vX
|LD Vx, Vy                | 
|LD $I, <label>           | Load address of data at <label> into register $I (most likely, before calling DRW)
|LDTR Vx                  |
|LDKR Vx                  |
|LDRT Vx                  | 
|LDRS Vx                  |    
|LDRI Vx                  |              
|LDAR Vx                  |
|LDRA Vx                  |               
|BCD Vx                   |
|ADD Vx, <byte>           |
|ADD Vx, Vy               |
|ADD I, Vx                |
|SUB Vx, Vy               |
|SUBN Vx, Vy              |
|OR Vx, Vy                |
|AND Vx, Vy               |
|XOR Vx, Vy               |
|SHR Vx                   |
|SHR Vx, Vy               |
|SHL Vx                   |
|SHL Vx, Vy               |
|RND Vx, <byte>           |
|SKP Vx                   |
|SKNP Vx                  |            
