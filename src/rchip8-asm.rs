extern crate regex;

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate log;
extern crate simple_logger;

#[cfg(test)]
extern crate tempfile;

mod common;

mod asm;
use asm::tokenizer::{Tokenizer, Token};
use asm::translator::Translator;


const OK: i32 = 0;
const ERROR: i32 = 1;


fn main() {
    simple_logger::init().unwrap();

    debug!("Starting...");

    let mut args = std::env::args();

    if args.len() != 3 {
        println!("Usage: rchip8-asm <SOURCE-FILE> <OUTPUT-ROM-FILE>");
        return;
    }

    // Eat the first argument (program name)
    args.next();

    let mut translator = Translator::from_file(&args.next().unwrap());

    let mut ret = OK;
    
    if let Err(e) = translator.translate() {
        println!("ERROR: {}", e);
        ret = ERROR;
    } else if let Err(e) = translator.save(&args.next().unwrap()) {
        println!("ERROR: {}", e);
        ret = ERROR;
    }

    std::process::exit(ret);
}
