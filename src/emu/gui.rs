use std::thread;
use std::sync::{Arc, Mutex};
use std::collections::HashMap;

use glium;
//use glium::{DisplayBuild, Surface};
use glium::Surface;
use glium::glutin::{Event, WindowEvent, KeyboardInput, VirtualKeyCode, ElementState};

use emu::framebuffer::RChip8DisplayBuffer;
use emu::input::RChip8Input;


const WIDTH: u32 = 64;
const HEIGHT: u32 = 32;
const SCALE: u32 = 10;


#[derive(Copy, Clone)]
struct Vertex {
    position: [f32; 2]
}

implement_vertex!(Vertex, position);

const VERTEX_SHADER: &'static str = r#"
    #version 140

    in vec2 position;
    uniform vec2 pos_mod;
    uniform mat4 perspective;

    void main() {
        gl_Position = perspective * vec4(position.x + pos_mod.x, position.y + pos_mod.y, 0.0, 1.0);
    }
"#;

const FRAGMENT_SHADER: &'static str = r#"
    #version 140

    out vec4 color;

    void main() {
        color = vec4(1.0, 1.0, 1.0, 1.0);
    }
"#;

pub struct RChip8GLGUI {
    __buffer: Arc<Mutex<RChip8DisplayBuffer>>,
    __input: Arc<Mutex<RChip8Input>>,
    __stop_switch: Arc<Mutex<bool>>,
    __render_thread: Option<thread::JoinHandle<()>>,
    __key_map: Arc<Mutex<HashMap<VirtualKeyCode, usize>>>
}

impl RChip8GLGUI {
    pub fn new(disp_buffer: Arc<Mutex<RChip8DisplayBuffer>>, input: Arc<Mutex<RChip8Input>>) -> RChip8GLGUI {
        let mut h = HashMap::new();

        h.insert(VirtualKeyCode::Key1, 0x0);
        h.insert(VirtualKeyCode::Key2, 0x1);
        h.insert(VirtualKeyCode::Key3, 0x2);
        h.insert(VirtualKeyCode::Key4, 0x3);
        h.insert(VirtualKeyCode::Q, 0x4);
        h.insert(VirtualKeyCode::W, 0x5);
        h.insert(VirtualKeyCode::E, 0x6);
        h.insert(VirtualKeyCode::R, 0x7);
        h.insert(VirtualKeyCode::A, 0x8);
        h.insert(VirtualKeyCode::S, 0x9);
        h.insert(VirtualKeyCode::D, 0xA);
        h.insert(VirtualKeyCode::F, 0xB);
        h.insert(VirtualKeyCode::Z, 0xC);
        h.insert(VirtualKeyCode::X, 0xD);
        h.insert(VirtualKeyCode::C, 0xE);
        h.insert(VirtualKeyCode::V, 0xF);

        RChip8GLGUI {
            __buffer: disp_buffer,
            __input: input,
            __stop_switch: Arc::new(Mutex::new(false)),
            __render_thread: None,
            __key_map: Arc::new(Mutex::new(h))
        }
    }

    pub fn start(&mut self) {
        let buf = self.__buffer.clone();
        let input = self.__input.clone();
        let key_map = self.__key_map.clone();
        let stop_sw = self.__stop_switch.clone();

        self.__render_thread = Some(thread::spawn( move || {
            let mut events_loop = glium::glutin::EventsLoop::new();

            let window = glium::glutin::WindowBuilder::new()
                .with_dimensions(WIDTH * SCALE, HEIGHT * SCALE)
                .with_title("RChip8");

            let context = glium::glutin::ContextBuilder::new();

            let display = glium::Display::new(window, context, &events_loop).unwrap();

            let square = vec![
                Vertex {position: [0.0, 0.0]},
                Vertex {position: [1.0, 0.0]},
                Vertex {position: [1.0, 1.0]},
                Vertex {position: [0.0, 1.0]},
                Vertex {position: [0.0, 0.0]},
            ];

            let perspective = [
                [ 2.0 / WIDTH as f32,  0.0,                  0.0, 0.0   ],
                [ 0.0,                -2.0 / HEIGHT as f32 , 0.0, 0.0   ],
                [ 0.0,                 0.0,                  1.0, 0.0   ],
                [-1.0,                 1.0,                  0.0, 1.0f32]
            ];

            let vb = glium::VertexBuffer::new(&display, &square).unwrap();
            let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);
            let program = glium::Program::from_source(&display, VERTEX_SHADER, FRAGMENT_SHADER, None).unwrap();

            loop {
                let mut target = display.draw();
                target.clear_color(0.0, 0.0, 0.0, 1.0);

                let mat = *buf.lock().unwrap().get_data();

                for i in 0..HEIGHT {
                    let exploded_bin: String = format!("{:064b}", mat[i as usize]);

                    let mut j = 0.0f32;

                    for ch in exploded_bin.chars() {
                        if ch == '1' {
                            target.draw(&vb, &indices, &program,
                                        &uniform!(pos_mod: [j, i as f32], perspective: perspective),
                                        &Default::default()).unwrap();
                        }
                        j += 1.0;
                    }
                }

                target.finish().unwrap();

                events_loop.poll_events(|ev| {
                    match ev {
                        //Event::WindowEvent::Closed | Event::KeyboardInput(_, _, Some(VirtualKeyCode::Escape)) => {
                        Event::WindowEvent { event, .. } => match event {
                            WindowEvent::Closed | WindowEvent::KeyboardInput { input: KeyboardInput { virtual_keycode: Some(VirtualKeyCode::Escape), .. }, .. } => {
                                (*input.lock().unwrap()).shut_down();
                            },
                            WindowEvent::KeyboardInput { input: KeyboardInput { state: es, virtual_keycode: Some(key), .. }, .. } => {
                                let km = &*key_map.lock().unwrap();

                                if km.contains_key(&key) {
                                    if es == ElementState::Pressed {
                                        (*input.lock().unwrap()).set_key(km[&key], true).ok();
                                    } else {
                                        (*input.lock().unwrap()).set_key(km[&key], false).ok();
                                    }
                                }
                            },
                            _ => {}
                        },
                        _ => {}
                    }
                });

                if *stop_sw.lock().unwrap() {
                    return;
                }
            }
        }));
    }

    pub fn end(&mut self) {
        match self.__render_thread {
            Some(_) => {
                *self.__stop_switch.lock().unwrap() = true;
                self.__render_thread.take().unwrap().join().unwrap();
            },
            None => {}
        };
    }
}

impl Drop for RChip8GLGUI {
    fn drop(&mut self) {
        self.end();
    }
}
