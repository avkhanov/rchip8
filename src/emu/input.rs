pub struct RChip8Input {
    __key_vals: [bool; 16],
    __power: bool
}

impl RChip8Input {
    pub fn new() -> RChip8Input {
        RChip8Input {
            __key_vals: [false; 16],
            __power: true
        }
    }

    pub fn get_key(&self, x: usize) -> bool {
        self.__key_vals[x]
    }

    pub fn set_key(&mut self, x: usize, val: bool) -> Result<usize, usize> {
        if x >= self.__key_vals.len() {
            Result::Err(x)
        } else {
            self.__key_vals[x] = val;
            Result::Ok(x)
        }
    }

    pub fn shut_down(&mut self) {
        self.__power = false;
    }

    pub fn is_on(&self) -> bool {
        self.__power
    }
}
