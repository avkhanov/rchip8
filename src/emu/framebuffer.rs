pub struct RChip8DisplayBuffer {
    __matrix: [u64; 32],
    //__key_vals: [bool; 16],
    //__key_map: HashMap<VirtualKeyCode, usize>
}

impl RChip8DisplayBuffer {
    pub fn new() -> RChip8DisplayBuffer {
        RChip8DisplayBuffer {
            __matrix: [0; 32],
        }
    }

    pub fn clear(&mut self) {
        for i in 0..self.__matrix.len() {
            self.__matrix[i] = 0;
        }
    }

    pub fn xor_row(&mut self, x: usize, y: usize, val: u8) -> bool {
        let mut ret = false;

        if y < self.__matrix.len() {
            let original = self.__matrix[y];
            let overlay = (val as u64).rotate_left((64 - (x + 1 + 7) % 64) as u32);

            if original & overlay > 0 {
                ret = true;
            }

            self.__matrix[y] = original ^ overlay;
        }

        ret
    }

    pub fn _dump(&self) {
        for i in self.__matrix.into_iter() {
            println!("{:064b}", i);
        }
    }

    pub fn get_data(&self) -> &[u64; 32] {
        &self.__matrix
    }

    /*
    pub fn process_key(&mut self, input: &VirtualKeyCode) {
        if self.__key_map.contains_key(input) {
            self.__key_vals[self.__key_map[input]] = true;
        }
    }

    pub fn clear_keys(&mut self) {
        self.__key_vals = [false; 16];
    }

    pub fn get_key(&self, x: usize) -> bool {
        self.__key_vals[x]
    }

    pub fn dump_keys(&self) {
        for i in 0..self.__key_vals.len() {
            print!("{:1X}: {} ", i, self.__key_vals[i]);
        }

        println!();
    }
    */
}