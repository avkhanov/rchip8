use std::io::Read;
use std::fs::File;
use std::mem::size_of_val;


pub struct RChip8Memory {
    __mem: [u8; 4096],
    __pc: usize,
    stack: [u16; 16],
    sp: usize
}

impl RChip8Memory {
    pub fn new(file: &str) -> RChip8Memory {
        let digits = [
            0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
            0x20, 0x60, 0x20, 0x20, 0x70, // 1
            0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
            0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
            0x90, 0x90, 0xF0, 0x10, 0x10, // 4
            0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
            0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
            0xF0, 0x10, 0x20, 0x40, 0x40, // 7
            0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
            0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
            0xF0, 0x90, 0xF0, 0x90, 0x90, // A
            0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
            0xF0, 0x80, 0x80, 0x80, 0xF0, // C
            0xE0, 0x90, 0x90, 0x90, 0xE0, // D
            0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
            0xF0, 0x80, 0xF0, 0x80, 0x80, // F
        ];

        let mut m = [0; 4096];

        for i in 0..digits.len() {
            m[i] = digits[i];
        }

        let mut ret = RChip8Memory { __mem: m, __pc: 0x200, stack: [0u16; 16], sp: 0 };
        ret.load_rom(file);
        ret.pc_jump(0x200);
        ret
    }

    pub fn load_rom(&mut self, file: &str) {
        let mut f = File::open(file).expect("Could not open ROM file");
        f.read(&mut self.__mem[0x200..4096]).expect("Could not load ROM into memory");
    }

    pub fn pc(&self) -> usize {
        self.__pc
    }

    pub fn get_digit_addr(&self, digit: usize) -> usize {
        // return self.__mem[digit * 5];
        return digit * 5
    }

    pub fn pc_jump(&mut self, addr: usize) {
        self.__pc = addr;
    }

    pub fn pc_incr(&mut self) {
        // Jump to next 16-bit instruction (skip over the low part of current command)
        self.__pc += 2;
    }

    pub fn op(&self) -> u16 {
        let upper: u16 = self.__mem[self.__pc] as u16;
        let lower: u16 = self.__mem[self.__pc + 1] as u16;

        (upper << 8) + lower
    }

    pub fn push(&mut self, val: u16) {
        self.stack[self.sp] = val;
        self.sp += 1;
    }

    pub fn pop(&mut self) -> u16 {
        self.sp -= 1;
        self.stack[self.sp]
    }

    pub fn get(&self, addr: usize) -> u8 {
        self.__mem[addr]
    }

    pub fn set(&mut self, addr: usize, value: u8) {
        self.__mem[addr] = value;
    }

    pub fn _dump(&self) {
        let mut i: usize = 0;

        while i < size_of_val(&self.__mem) {
            if i % 64 == 0 {
                println!();
            }

            print!("{:02X}{:02X} ", self.__mem[i], self.__mem[i + 1]);
            i += 2;
        }
    }
}