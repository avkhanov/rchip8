extern crate rand;

use std;
use std::sync::{Arc, Mutex, MutexGuard};
use std::thread;
use std::time::{SystemTime, Duration};

use emu::framebuffer::RChip8DisplayBuffer;
use emu::mem::RChip8Memory;
use emu::input::RChip8Input;

use common::opcode::OpCode;

pub struct RChip8CPU {
    dispbuf: Arc<Mutex<RChip8DisplayBuffer>>,
    memory: Arc<Mutex<RChip8Memory>>,
    input: Arc<Mutex<RChip8Input>>,
    v: [u8; 16],
    i: u16,
    dt: Arc<Mutex<u8>>,
    st: Arc<Mutex<u8>>,
}

impl RChip8CPU {
    pub fn new(disp: Arc<Mutex<RChip8DisplayBuffer>>,
    memory: Arc<Mutex<RChip8Memory>>,
    input: Arc<Mutex<RChip8Input>>
    ) -> RChip8CPU {
        RChip8CPU {
            dispbuf: disp,
            memory: memory,
            input: input,
            v: [0u8; 16],
            i: 0u16,
            dt: Arc::new(Mutex::new(0u8)),
            st: Arc::new(Mutex::new(0u8)),
        }
    }

    fn mem(&self) -> MutexGuard<RChip8Memory> {
        self.memory.lock().unwrap()
    }

    fn disp(&self) -> MutexGuard<RChip8DisplayBuffer> {
        self.dispbuf.lock().unwrap()
    }

    fn input(&self) -> MutexGuard<RChip8Input> {
        self.input.lock().unwrap()
    }

    pub fn start(&mut self) {
        let dt_ref = self.dt.clone();
        let input_ref = self.input.clone();

        let dt_thread = thread::spawn(move || {
            let mut last_time = SystemTime::now();
            let delay = Duration::new(0u64, 1_000_000_000 / 60);

            while (*input_ref.lock().unwrap()).is_on() {
                if last_time.elapsed().unwrap() > delay {
                    let mut dt = dt_ref.lock().unwrap();
                    if *dt > 0 {
                        *dt -= 1;
                    }

                    last_time = SystemTime::now();
                }
            }
        });

        while (*self.input.lock().unwrap()).is_on() {
            let op = OpCode::from_u16(self.mem().op());

            match op {
                OpCode(0x0, 0x0, 0xE, 0x0) => self.cls(), // CLS
                OpCode(0x0, 0x0, 0xE, 0xE) => self.ret(), // RET
                OpCode(0x1,  n1,  n2,  n3) => self.jump(OpCode::join_three(n1, n2, n3)), // JP addr
                OpCode(0x2,  n1,  n2,  n3) => self.call(OpCode::join_three(n1, n2, n3)), // CALL addr
                OpCode(0x3,   x,  k1,  k2) => self.skip_if_vx_eq(x as usize, OpCode::join_two(k1, k2)), // SE Vx, byte
                OpCode(0x4,   x,  k1,  k2) => self.skip_if_vx_ne(x as usize, OpCode::join_two(k1, k2)), // SNE Vx, byte
                OpCode(0x5,   x,   y, 0x0) => self.skip_if_vx_eq_vy(x as usize, y as usize), // SE Vx, Vy
                OpCode(0x6,   x,  k1,  k2) => self.load_to_vx(x as usize, OpCode::join_two(k1, k2)), // LD Vx, byte
                OpCode(0x7,   x,  k1,  k2) => self.add_to_vx(x as usize, OpCode::join_two(k1, k2)), // ADD Vx, byte
                OpCode(0x8,   x,   y, 0x0) => self.load_vy_to_vx(x as usize, y as usize), // LD Vx, Vy
                OpCode(0x8,   x,   y, 0x1) => self.or_vy_to_vx(x as usize, y as usize), // OR Vx, Vy
                OpCode(0x8,   x,   y, 0x2) => self.and_y_to_x(x as usize, y as usize), // AND Vx, Vy
                OpCode(0x8,   x,   y, 0x3) => self.xor_vx_vy(x as usize, y as usize), // XOR Vx, Vy
                OpCode(0x8,   x,   y, 0x4) => self.add_y_to_x(x as usize, y as usize), // ADD Vx, Vy
                OpCode(0x8,   x,   y, 0x5) => self.sub_vy_from_vx(x as usize, y as usize), // SUB Vx, Vy
                OpCode(0x8,   x,   y, 0x6) => self.shift_right(x as usize, y as usize), // SHR Vx {, Vy}
                OpCode(0x8,   x,   y, 0x7) => self.sub_vx_from_vy(x as usize, y as usize), // SUBN Vx, Vy
                OpCode(0x8,   x,   y, 0xE) => self.shift_left(x as usize, y as usize), // SHL Vx {, Vy}
                OpCode(0x9,   x,   y, 0x0) => self.skip_if_vx_ne_vy(x as usize, y as usize), // SNE Vx, Vy
                OpCode(0xA,  n1,  n2,  n3) => self.load_to_i(OpCode::join_three(n1, n2, n3)), // LD I, addr
                OpCode(0xB,  n1,  n2,  n3) => self.jump_to_addr_plus_v0(OpCode::join_three(n1, n2, n3)), // JP V0, addr
                OpCode(0xC,   x,  k1,  k2) => self.rand_and(x as usize, OpCode::join_two(k1, k2)), // RND Vx, byte
                OpCode(0xD,   x,   y,   n) => self.draw_sprite_at_i(x as usize, y as usize, n), // DRW Vx, Vy, nibble
                OpCode(0xE,   x, 0x9, 0xE) => self.skip_if_key_vx_pressed(x as usize), // SKP Vx
                OpCode(0xE,   x, 0xA, 0x1) => self.skip_if_key_vx_not_pressed(x as usize), // SKNP Vx
                OpCode(0xF,   x, 0x0, 0x7) => self.load_dt_to_vx(x as usize), // LD Vx, DT
                OpCode(0xF,   x, 0x0, 0xA) => self.wait_for_key(x as usize), // LD Vx, K
                OpCode(0xF,   x, 0x1, 0x5) => self.load_vx_to_dt(x as usize), // LD DT, Vx
                OpCode(0xF,   x, 0x1, 0x8) => self.set_st_to_vx(x as usize), // LD ST, Vx
                OpCode(0xF,   x, 0x1, 0xE) => self.add_vx_to_i(x as usize), // ADD I, Vx
                OpCode(0xF,   x, 0x2, 0x9) => self.set_i_to_sprite_digit_vx(x as usize), // LD F, Vx
                OpCode(0xF,   x, 0x3, 0x3) => self.bcd_of_vx_to_mem_at_i(x as usize), // LD B, Vx
                OpCode(0xF,   x, 0x5, 0x5) => self.copy_v0_through_vx_to_mem_at_i(x as usize), // LD [I], Vx
                OpCode(0xF,   x, 0x6, 0x5) => self.load_v0_through_vx_from_mem_at_i(x as usize), // LD Vx, [I]
                _ => {
                    panic!("Instruction not implemented: {}", op);
                }
            }

            std::thread::sleep(Duration::new(0, 1_000_000));
        }

        dt_thread.join().unwrap();
    }


    fn cls(&mut self) {
        self.disp().clear();
        self.mem().pc_incr();
    }

    fn load_to_i(&mut self, n: u16) {
        self.i = n;
        self.mem().pc_incr();
    }

    fn jump_to_addr_plus_v0(&mut self, n: u16) {
        let addr = (n + self.v[0] as u16) as usize;
        self.mem().pc_jump(addr);
    }

    fn jump(&mut self, addr: u16) {
        self.mem().pc_jump(addr as usize);
    }

    fn skip_if_vx_eq(&mut self, x: usize, byte: u8) {
        if self.v[x] == byte {
            self.mem().pc_incr();
        }

        self.mem().pc_incr();
    }

    fn skip_if_vx_ne(&mut self, x: usize, byte: u8) {
        if self.v[x] != byte {
            self.mem().pc_incr();
        }

        self.mem().pc_incr();
    }

    fn skip_if_vx_eq_vy(&mut self, x: usize, y: usize) {
        if self.v[x] == self.v[y] {
            self.mem().pc_incr();
        }
    }

    fn skip_if_vx_ne_vy(&mut self, x: usize, y: usize) {
        if self.v[x] != self.v[y] {
            self.mem().pc_incr();
        }

        self.mem().pc_incr();
    }

    fn call(&self, n: u16) {
        let pc = self.mem().pc() as u16;
        self.mem().push(pc);
        self.mem().pc_jump(n as usize);
    }

    fn ret(&mut self) {
        let new_pc = self.mem().pop() as usize;
        self.mem().pc_jump(new_pc);
        self.mem().pc_incr();
    }

    fn load_to_vx(&mut self, x: usize, byte: u8) {
        self.v[x] = byte;
        self.mem().pc_incr();
    }

    fn add_to_vx(&mut self, x: usize, byte: u8) {
        if x == 1 {
            //self.disp().debug(&format!(">>> {} + {} = {}", self.v[x], byte, self.v[x].wrapping_add(byte)));
        }
        self.v[x] = self.v[x].wrapping_add(byte);
        self.mem().pc_incr();
    }

    fn add_vx_to_i(&mut self, x: usize) {
        self.i = self.i.wrapping_add(self.v[x] as u16);
        self.mem().pc_incr();
    }

    fn sub_vy_from_vx(&mut self, x: usize, y: usize) {
        let val = self.v[x].wrapping_sub(self.v[y]);

        if val > self.v[x] {
            self.v[0xF] = 1;
        } else {
            self.v[0xF] = 0;
        }

        self.v[x] = val;
        self.mem().pc_incr();
    }

    fn sub_vx_from_vy(&mut self, x: usize, y: usize) {
        let val = self.v[y].wrapping_sub(self.v[x]);

        if val > self.v[y] {
            self.v[0xF] = 1;
        } else {
            self.v[0xF] = 0;
        }

        self.v[x] = val;
        self.mem().pc_incr();
    }

    fn shift_right(&mut self, x: usize, y: usize) {
        let carry_flag = self.v[y] & 0x1u8 == 0x1u8;
        self.v[x] = self.v[y] >> 1;

        if carry_flag {
            self.v[0xF] = 1;
        } else {
            self.v[0xF] = 0;
        }

        self.mem().pc_incr();
    }

    fn shift_left(&mut self, x: usize, y: usize) {
        let carry_flag = self.v[y] & 0x80u8 == 0x80u8;
        self.v[x] = self.v[y] << 1;

        if carry_flag {
            self.v[0xF] = 1;
        } else {
            self.v[0xF] = 0;
        }

        self.mem().pc_incr();
    }

    fn xor_vx_vy(&mut self, x: usize, y: usize) {
        self.v[x] = self.v[x] ^ self.v[y];
        self.mem().pc_incr();
    }

    fn rand_and(&mut self, x: usize, byte: u8) {
        self.v[x] = rand::random::<u8>() & byte;
        self.mem().pc_incr();
    }

    fn draw_sprite_at_i(&mut self, x: usize, y: usize, num_rows: u8) {
        let mut collision = false;
        let addr = self.i;

        for i in 0..num_rows {
            collision |= self.disp().xor_row(
                self.v[x] as usize, self.v[y] as usize + (i as usize),
                self.mem().get((addr + (i as u16)) as usize)
            );
        }

        //self.disp().debug(&format!("COLLISION: {}", collision));
        if collision {
            self.v[0xF] = 1;
        } else {
            self.v[0xF] = 0;
        }

        self.mem().pc_incr();
    }

    fn load_vx_to_dt(&mut self, x: usize) {
        //self.disp().debug(&format!("DT: {}; V{}: {}", *(self.dt.lock().unwrap()), x, self.v[x]));
        *(self.dt.lock().unwrap()) = self.v[x];
        self.mem().pc_incr();
    }

    fn load_dt_to_vx(&mut self, x: usize) {
        self.v[x] = *(self.dt.lock().unwrap());
        //self.disp().debug(&format!("DT: {}; V{}: {}", *(self.dt.lock().unwrap()), x, self.v[x]));
        if self.v[x] == 0 {
            //self.disp().debug(&format!("IT IS ZERO!!! DT: {}; V{}: {}", *(self.dt.lock().unwrap()), x, self.v[x]));
        }
        self.mem().pc_incr();
    }

    fn load_vy_to_vx(&mut self, x: usize, y: usize) {
        self.v[x] = self.v[y];
        self.mem().pc_incr();
    }

    fn copy_v0_through_vx_to_mem_at_i(&mut self, x: usize) {
        for j in 0..(x + 1) {
            self.mem().set((self.i as usize) + j, self.v[j]);
        }

        self.mem().pc_incr();
    }

    fn bcd_of_vx_to_mem_at_i(&mut self, x: usize) {
        let mut rem = x as u8;

        self.mem().set(self.i as usize, rem / 100);
        rem %= 100;
        self.mem().set((self.i + 1) as usize, rem / 10);
        rem %= 10;
        self.mem().set((self.i + 2) as usize, rem);

        self.mem().pc_incr();
    }

    fn load_v0_through_vx_from_mem_at_i(&mut self, x: usize) {
        for j in 0..(x + 1) {
            let vj = self.mem().get(self.i as usize + j);
            self.v[j] = vj;
        }

        self.mem().pc_incr();
    }

    fn set_i_to_sprite_digit_vx(&mut self, x: usize) {
        let i = self.mem().get_digit_addr(self.v[x] as usize) as u16;
        self.i = i;
        self.mem().pc_incr();
    }

    fn and_y_to_x(&mut self, x: usize, y: usize) {
        self.v[x] &= self.v[y];
        self.mem().pc_incr();
    }

    fn or_vy_to_vx(&mut self, x: usize, y: usize) {
        self.v[x] = self.v[x] | self.v[y];
        self.mem().pc_incr();
    }

    fn add_y_to_x(&mut self, x: usize, y: usize) {
        let val = self.v[x].wrapping_add(self.v[y]);

        if val < self.v[x] {
            self.v[0xF] = 1;
        } else {
            self.v[0xF] = 0;
        }

        self.v[x] = val;
        self.mem().pc_incr();
    }

    fn skip_if_key_vx_not_pressed(&mut self, x: usize) {
        if !self.input().get_key(self.v[x] as usize) {
            self.mem().pc_incr();
        }

        self.mem().pc_incr();
    }

    fn skip_if_key_vx_pressed(&self, x: usize) {
        if self.input().get_key(self.v[x] as usize) {
            self.mem().pc_incr();
        }
        self.mem().pc_incr();
    }

    fn wait_for_key(&mut self, x: usize) {
        let mut key = 0u8;

        'outer: while self.input().is_on() {
            let keys = self.input();

            for i in 0..16 {
                if keys.get_key(i) {
                    key = i as u8;
                    break 'outer;
                }
            }
        }

        self.v[x] = key;
    }

    fn set_st_to_vx(&mut self, x: usize) {
        //TODO: Implement sound & ST
        *self.st.lock().unwrap() = self.v[x];
        self.mem().pc_incr();
    }
}
