use std;

pub struct OpCode (pub u8, pub u8, pub u8, pub u8);

impl OpCode {
    pub fn from_u16(val: u16) -> OpCode {
        let a: u8 = (val >> 12) as u8;
        let b: u8 = ((val & 0x0FFF) >> 8) as u8;
        let c: u8 = ((val & 0x00FF) >> 4) as u8;
        let d: u8 = (val & 0x000F) as u8;

        OpCode(a, b, c, d)
    }

    pub fn join_two(fourbit_1: u8, fourbit_2: u8) -> u8 {
        (fourbit_1 << 4) + (fourbit_2 & 0x0F)
    }

    pub fn join_three(fourbit_1: u8, fourbit_2: u8, fourbit_3: u8) -> u16 {
        ((fourbit_1 as u16) << 8) + ((fourbit_2 as u16 & 0x000F) << 4) + (fourbit_3 as u16 & 0x000F)
    }

    pub fn to_u16(&self) -> u16 {
        ((self.0 as u16) << 12) + ((self.1 as u16) << 8) + ((self.2 as u16) << 4) + (self.3 as u16)
    }
}

impl std::fmt::Display for OpCode {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "[{:X} {:X} {:X} {:X}]", self.0, self.1, self.2, self.3)
    }
}
