use std::collections::hash_map::HashMap;
use std::iter::Iterator;
use std::io::Write;
use common::opcode::OpCode;

const N_SIZE: usize = 1;
const MEMREF_SIZE: usize = 3;
const N_PER_MEM_ADDR: usize = 2;
const MEM_OFFSET: usize = 0x200;

#[derive(Debug)]
pub enum Seq {
    N(char),
    MemRef(String),
    MemSlot(String)
}

pub struct Layout {
    __layout: Vec<Seq>,
    __mem: HashMap<String, usize>,
    __n_ptr: usize,
    __op_buf: u16
}

impl Layout {
    pub fn new() -> Layout {
        Layout {
            __layout: Vec::new(),
            __mem: HashMap::new(),
            __n_ptr: 0,
            __op_buf: 0
        }
    }

    pub fn add(&mut self, value: Seq) {
        debug!("Adding {:?} to layout", value);

        match value {
            c @ Seq::N(_) => { self.__layout.push(c); self.__n_ptr += N_SIZE; }
            Seq::MemSlot(ref s) => { self.__mem.insert(s.clone(), self.__n_ptr / N_PER_MEM_ADDR); },
            c @ Seq::MemRef(_) => { self.__layout.push(c); self.__n_ptr += MEMREF_SIZE; }
        };
    }

    pub fn dump(&self) {
        println!("\nSymbols:");

        for (k, v) in self.__mem.iter() {
            println!("\t{:?}: {:?}", k, v);
        }

        println!("\nHex Map:");

        for v in self.__layout.iter() {
            println!("\t{:?}", v);
        }

    }

    pub fn peek_last_word(&self) -> Option<String> {
        let mut ret: u16 = 0;
        let mut nibbles_to_find = 4;
        let mut index = self.__layout.len() - 1;

        if index <= 0 {
            return None;
        }
        
        while nibbles_to_find > 0 {
            if let Some(s) = self.__layout.get(index) {
                match s {
                    &Seq::N(c) => {
                        ret += (c.to_digit(16).unwrap() as u16) << (4 - nibbles_to_find) * 4;
                        nibbles_to_find -= N_SIZE; }
                    &Seq::MemRef(ref mr) => {
                        let addr = OpCode::from_u16((MEM_OFFSET + self.__mem[mr]) as u16);
                        ret += addr.to_u16() << (4 - nibbles_to_find) * 4;
                        nibbles_to_find -= MEMREF_SIZE; }
                    c @ _ =>  { debug!("Unsupported segment: {:?}", c); }
                }

                if index > 0 {
                    index -= 1;
                } else if nibbles_to_find > 0 {
                    return None;
                }

            } else {
                return None;
            }
        }

        return Some(format!("{:04X}", ret));
    }

    pub fn write<T: Write>(&self, buf: &mut T) -> Result<(), String> {
        let mut i = self.__layout.iter();
        let mut ret = Ok(());

        loop {
            match (i.next(), i.next()) {
                (Some(&Seq::N(c1)), Some(&Seq::N(c2))) => {
                    buf.write(&[OpCode::join_two(u8::from_str_radix(&(c1.to_string()), 16).unwrap(), u8::from_str_radix(&(c2.to_string()), 16).unwrap())]);
                },
                (Some(&Seq::N(c)), Some(&Seq::MemRef(ref s))) => {
                    println!("2nd");
                    let com = u8::from_str_radix(&(c.to_string()), 16).unwrap();
                    let loc = OpCode::from_u16((MEM_OFFSET + self.__mem[s]) as u16);
                    buf.write(&[OpCode::join_two(com, loc.1)]);
                    buf.write(&[OpCode::join_two(loc.2, loc.3)]);
                    println!("LOC: {}", loc);
                },
                (_, None) => {println!("3rd"); break}
                c @ _ => {println!("4th: {:?}", c);}
            };
        }

        ret
    }
}
