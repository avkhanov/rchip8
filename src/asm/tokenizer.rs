use std::io::{BufRead, BufReader, Read, SeekFrom, Seek};
use std::fs::File;
use std::iter::Iterator;
use regex::{Regex, Match};


const ch0: char = 0 as char;


#[derive(Debug, Clone)]
pub enum Token {
    Comment    {line: usize, column: usize, value: String},
    Label      {line: usize, column: usize, value: String},
    Identifier {line: usize, column: usize, value: String},
    Register   {line: usize, column: usize, value: String},
    Number     {line: usize, column: usize, value: u8},
    Comma      {line: usize, column: usize, value: String},
    Whitespace {line: usize, column: usize, value: String},
    End        {line: usize, column: usize},
    Error      {line: usize, column: usize, value: String}
}

pub struct Tokenizer {
    __buf: BufReader<File>,
    __line: usize,
    __col: usize
}

impl Tokenizer {
    pub fn from_file(filename: &str) -> Tokenizer {
        let mut buf = BufReader::new(File::open(filename).expect("Could not open file"));
        let mut cur_line = String::new();
        //buf.read_line(&mut cur_line);

        let mut ret = Tokenizer {
            __buf: buf,
            __line: 0,
            __col: 0
        };

        ret
    }

    fn consume(&mut self) -> (usize, char) {
        let mut buf = [0u8];
        match self.__buf.read(&mut buf) {
            Err(err) => panic!("{}", err),
            Ok(_) => {
                if buf[0] as char == '\n' {
                    self.__line += 1;
                    self.__col = 0;
                }

                self.__col += 1;
                (self.__col, buf[0] as char)
            }
        }
    }

    fn peek(&mut self) -> (usize, char) {
        let mut buf = [0u8];
        let ret = match self.__buf.read(&mut buf) {
            Err(err) => panic!("{}", err),
            Ok(0) => (self.__col, '\u{0}'),
            Ok(n) => { self.__buf.seek(SeekFrom::Current(-1)); (self.__col, buf[0] as char) }
        };

        ret
    }

    fn unexpected_char(&self, col: usize, c: char) -> Option<Token> {
        println!("{}:{} - Unknown character: {:?}", self.__line, col, c);
        None
    }

    fn start(&mut self) -> Option<Token> {
        let (col, chr) = self.peek();

        match chr {
            ' ' | '\t' | '\n' | '\r'  => self.whitespace(),
            '#' => self.comment(),
            '$' => self.register(),
            ',' => self.comma(),
            c @ 'a'...'z' | c @ 'A'...'Z' | c @ '_' => self.id_or_label(),
            c @ '0'...'9' => self.number(),
            '\u{0}' => None,
            c @ _ => self.unexpected_char(col, c)
        }
    }

    fn whitespace(&mut self) -> Option<Token> {
        let mut s = String::new();

        loop {
            match self.peek() {
                (_, c @ ' ') | (_, c @ '\t') | (_, c @ '\n') | (_, c @ '\r') => { s.push(c); self.consume(); },
                _ => break
            }
        }

        Some(Token::Whitespace {line: self.__line, column: self.__col, value: s})
    }

    fn comma(&mut self) -> Option<Token> {
        let mut s = String::with_capacity(1);
        let (col, ch) = self.consume();
        s.push(ch);

        Some(Token::Comma {line: self.__line, column: col, value: s})
    }

    fn comment(&mut self) -> Option<Token> {
        let mut s = String::new();
        self.consume(); // Consume the '#'

        let (mut col, mut c) = self.peek();

        while c != '\n' && c != (0 as char) {
            s.push(c);
            self.consume();
            let (new_col, new_c) = self.peek();
            col = new_col;
            c = new_c;
        }

        Some(Token::Comment {line: self.__line, column: col, value: s})
    }

    fn id_or_label(&mut self) -> Option<Token> {
        let mut s = String::new();
        let (mut col, mut ch) = self.peek();

        loop {
            match ch {
                ':' => {self.consume(); return Some(Token::Label {line: self.__line, column: col, value: s})},
                c @ 'a'...'z' | c @ 'A'...'Z' | c @ '0'...'9' | c @ '_' => {s.push(c); self.consume()},
                _ => break
            };

            let (new_col, new_ch) = self.peek();
            col = new_col;
            ch = new_ch;
        }

        Some(Token::Identifier {line: self.__line, column: self.__col, value: s})
    }

    fn register(&mut self) -> Option<Token> {
        let mut s = String::new();
        self.consume(); // Consume the '$'

        let (mut col, mut c) = self.peek();

        loop{
            match self.peek() {
                (_, c @ 'a'...'z') | (_, c @ 'A'...'Z') | (_, c @ '0'...'9') => {s.push(c); self.consume();},
                _ => break,
            }
        }

        Some(Token::Register {line: self.__line, column: self.__col, value: s})
    }

    fn number(&mut self) -> Option<Token> {
        let (mut col, mut c) = self.consume();

        match c {
            '0' => match self.peek() {
                (_, 'b') => {self.consume(); self.binary()},
                (_, 'x') => {self.consume(); self.hex()},
                _ => self.decimal(c)
            },
            '1'...'9' => self.decimal(c),
            _ => None
        }
    }

    fn decimal(&mut self, first: char) -> Option<Token> {
        let mut s = String::new();
        s.push(first);

        loop {
            match self.peek() {
                (_, c @ '0'...'9') => { s.push(c); self.consume(); },
                _ => break
            }
        }

        if let Ok(num) = u8::from_str_radix(&s, 10) {
            Some(Token::Number {line: self.__line, column: self.__col, value: num})
        } else {
            Some(Token::Error {line: self.__line, column: self.__col, value: format!("Number does not fit in one byte")})
        }
    }

    fn binary(&mut self) -> Option<Token> {
        let mut s = String::new();

        loop {
            match self.peek() {
                (_, c @ '0'...'1') => { s.push(c); self.consume(); },
                _ => break
            }
        }

        if let Ok(num) = u8::from_str_radix(&s, 2) {
            Some(Token::Number {line: self.__line, column: self.__col, value: num})
        } else {
            Some(Token::Error {line: self.__line, column: self.__col, value: format!("Number does not fit in one byte")})
        }
    }

    fn hex(&mut self) -> Option<Token> {
        let mut s = String::new();

        loop {
            match self.peek() {
                (_, c @ '0'...'9') => { s.push(c); self.consume(); },
                (_, c @ 'a'...'f') => { s.push(c); self.consume(); },
                (_, c @ 'A'...'F') => { s.push(c); self.consume(); },
                _ => break
            }
        }

        if let Ok(num) = u8::from_str_radix(&s, 16) {
            Some(Token::Number {line: self.__line, column: self.__col, value: num})
        } else {
            Some(Token::Error {line: self.__line, column: self.__col, value: format!("Number does not fit in one byte")})
        }
    }
}


impl Iterator for Tokenizer {
    type Item = Token;

    fn next(&mut self) -> Option<Token> {
        self.start()
    }
}
