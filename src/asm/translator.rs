use asm::tokenizer::{Tokenizer, Token};
use asm::tokenizer::Token::{Comment, Label, Identifier, Register, Number, Comma, Whitespace, End, Error};
use asm::layout::{Seq, Layout};
use asm::layout::Seq::{N, MemSlot, MemRef};
use std::fs::File;
use common::opcode::OpCode;

type TranslateResult = Result<(), String>;

pub struct Translator {
    __tokenizer: Tokenizer,
    __layout: Layout,
    __peek: Option<Token>
}

impl Translator {
    pub fn from_file(filename: &str) -> Translator {
        debug!("Loading asm source file '{}'", filename);
        Translator {
            __tokenizer: Tokenizer::from_file(filename),
            __layout: Layout::new(),
            __peek: None
        }
    }

    pub fn peek(&mut self) -> Option<Token> {
        if self.__peek.is_none() {
            loop {
                match self.__tokenizer.next() {
                    Some(Whitespace {..}) | Some(Comment {..}) => {}
                    c @ _ => { self.__peek = c; break; }
                }
                //self.__peek = self.__tokenizer.next();
            }
        }

        self.__peek.clone()
    }

    pub fn consume(&mut self) -> Option<Token> {
        self.peek();
        self.__peek.take()

    }

    pub fn get_layout(&self) -> &Layout {
        &self.__layout
    }

    pub fn translate(&mut self) -> TranslateResult {
        let mut done = false;
        let mut ret = Ok(());

        while !done {
            ret = match self.peek() {
                Some(ref x) => match x {
                    &Label {..} =>  self.label(x),
                    &Identifier {..} => self.identifier(x),
                    &Number {..} => self.number(x),
                    t @ _ => Err(format!("Expected: Label, Number, or Identifier; Found: {:?}", t))
                },
                None => { done = true; Ok(()) }
            };

            match ret {
                Err(_) => break,
                _ => ()
            };
        }
        
        ret
    }

    pub fn save(&mut self, save_file: &str) -> TranslateResult {
        let mut file = File::create(save_file);

        match file {
            Ok(ref mut f) => { self.__layout.write(f); Ok(()) },
            Err(_) => Err("Could not save file".to_string())
        }
    }

    fn label(&mut self, v: &Token) -> TranslateResult {
        println!("label");

        match v {
            &Label { value: ref s, ..} => self.__layout.add(MemSlot(s.clone())),
            _ => panic!("Not a label!")
        }

        self.consume();

        return match self.peek() {
            Some(ref x) => match x {
                &Whitespace { .. } | &Comment { .. } => { self.consume(); Ok(()) },
                &Identifier { .. } => self.identifier(x),
                &Number {..} => self.number(x),
                t @ _ => Err(format!("Expected: Identifier or Number; Found: {:?}", t))
            },
            None => Ok(())
        };
    }

    fn identifier(&mut self, v: &Token) -> TranslateResult {
        println!("identifier");
        match v {
            &Identifier { value: ref s, ..} => match &s[..] {
                "cls" => self.cls(),
                "ld" => self.ld(),
                "jp" => self.jp(),
                "ldri" => self.ldri(),
                "ldtr" => self.ldtr(),
                "ldkr" => self.ldkr(),
                "ldrt" => self.ldrt(),
                "ldrs" => self.ldrs(),
                "ldar" => self.ldar(),
                "ldra" => self.ldra(),
                "drw" => self.drw(),
                "call" => self.call(),
                "ret" => self.ret(),
                "add" => self.add(),
                "sne" => self.se_or_sne(false),
                "se" => self.se_or_sne(true),
                "bcd" => self.bcd(),
                "add" => self.add(),
                "sub" => self.sub(),
                "subn" => self.subn(),
                "or" => self.or(),
                "xor" => self.xor(),
                "and" => self.and(),
                "shr" => self.shr(),
                "shl" => self.shl(),
                "rnd" => self.rnd(),
                "skp" => self.skp_or_sknp(true),
                "sknp" => self.skp_or_sknp(false),
                _ => Err(format!("Unknown opcode: {}", s)),
            },
            _ => Err("Not an identifier!".to_string())
        }
    }

    fn register(&mut self, v: &Token) -> TranslateResult {
        println!("register");
        self.consume();

        Ok(())
    }

    fn comma(&mut self, v: &Token) -> TranslateResult {
        println!("comma");
        self.consume();

        Ok(())
    }

    fn number(&mut self, v: &Token) -> TranslateResult {
        println!("number");
        self.consume();

        match v {
            &Number { value: ref val, .. } => {
                for c in format!("{:02X}", val).chars() {
                    self.__layout.add(N(c));
                }

                Ok(())
            },
            c @ _ => Err(format!("Expected a number, found: {:?}", c))
        }
    }

    // OPCODES
    fn cls(&mut self) -> TranslateResult {
        debug!("cls");
        self.consume();
        self.__layout.add(N('0'));
        self.__layout.add(N('0'));
        self.__layout.add(N('E'));
        self.__layout.add(N('0'));

        Ok(())
    }

    fn bcd(&mut self) -> TranslateResult {
        debug!("bcd");
        self.consume();

        self.expect_register().and_then(|r: char| -> TranslateResult {
            match r {
                'i' | 'I' => Err(format!("Register I not supported for this operation")),
                _ => {
                    self.__layout.add(N('F'));
                    self.__layout.add(N(r));
                    self.__layout.add(N('3'));
                    self.__layout.add(N('3'));

                    Ok(())
                }
            }
        })
    }

    fn ld(&mut self) -> TranslateResult {
        debug!("ld");
        self.consume();
        match self.peek() {
            Some(ref x) => match x {
                &Register {..} => self.ld_reg(),
                c @ _ => Err(format!("Expected a register, found: {:?}", c))
            },
            None => Err("Missing operand".to_string())
        }
    }

    fn ld_reg(&mut self) -> TranslateResult {
        debug!("ld_reg");

        self.expect_register().and_then(|c: char| -> TranslateResult {
            self.expect_comma().and_then(|()| -> TranslateResult {
                match c {
                    '0'...'9' | 'a'...'f' | 'A'...'F' => {
                        self.expect_number().and_then(|val: u8| -> TranslateResult {
                            self.__layout.add(N('6'));
                            self.__layout.add(N(c));

                            for c in format!("{:02X}", val).chars() {
                                self.__layout.add(N(c));
                            }

                            Ok(())
                        }).or_else(|_: String| -> TranslateResult {
                           self.expect_register().and_then(|reg2: char| -> TranslateResult { 
                                match reg2 {
                                    '0'...'9' | 'a'...'f' | 'A'...'F' => {
                                        self.consume();
                                        self.__layout.add(N('8'));
                                        self.__layout.add(N(c));
                                        self.__layout.add(N(reg2));
                                        self.__layout.add(N('0'));

                                        Ok(())
                                    },
                                     _ => Err(format!("Register {} not supported by this command", reg2))
                                }
                            })
                        }).or_else(|_: String| -> TranslateResult {
                            Err("Missing operand".to_string())
                        })
                    },
                    'i' | 'I' => match self.peek() {
                        Some(ref x) => match x {
                            &Identifier { value: ref s, .. } => {
                                self.consume();
                                self.__layout.add(N('A'));
                                self.__layout.add(MemRef(s.clone()));

                                Ok(())
                            },
                            c @ _ => Err(format!("Expected Identifier, found: {:?}", c))
                        },
                        None => Err("Missing operand".to_string())
                    },
                    _ => Err(format!("Only registers V0 - VF, I are allowed for this command. Found: {}", c))
                }
            })
        })


        //Ok(())
    }

    fn expect_comma(&mut self) -> TranslateResult {
        debug!("Expecting comma");
        match self.peek() {
            Some(ref x) => match x {
                &Comma {..} => {self.consume(); Ok(())},
                c @ _ => Err(format!("Expected a comma, found: {:?}", c))
            },
            None => Err("Expected a comma, found: EOF".to_string())
        }
    }

    fn expect_eol(&mut self) -> TranslateResult {
        debug!("Expecting end of line");
        match self.peek() {
            Some(ref x) => Err(format!("Expected end of line, found: {:?}", x)),
            None => Ok(())
        }
    }

    fn jp(&mut self) -> TranslateResult {
        debug!("jp");

        self.consume();
        
        self.expect_identifier().and_then(|label: String| -> TranslateResult {
            self.__layout.add(N('1'));
            self.__layout.add(MemRef(label));
            Ok(())
        }).or_else(|_: String| -> TranslateResult {
            self.expect_register().and_then(|c: char| -> TranslateResult {
                match c {
                    '0' => {
                        self.expect_comma().and_then(|()| -> TranslateResult {
                            self.expect_identifier().and_then(|label: String| -> TranslateResult {
                                self.__layout.add(N('B'));
                                self.__layout.add(MemRef(label));
                                Ok(())
                            })
                        })
                    },
                    _ => Err(format!("This command only supports register V0. Encountered: V{}", c))
                }
            })
        })/*.or_else(|_: String| -> TranslateResult {
            Err(format!("Invalid syntax for JP. Supported: 'JP <LABEL>', 'JP <REG> <LABEL>'"))
        })*/
    }

    fn ldri(&mut self) -> TranslateResult {
        println!("ldri");

        self.consume();
        self.expect_register().and_then(|c: char| -> TranslateResult {
            match c {
                '0'...'9' | 'a'...'f' | 'A'...'F' => {
                    self.__layout.add(N('F'));
                    self.__layout.add(N(c));
                    self.__layout.add(N('2'));
                    self.__layout.add(N('9'));

                    Ok((()))
                },
                _ => Err(format!("Only registers V0 - VF are allowed for this command; Found: {}", c))
            }
        })
    }

    fn ldtr(&mut self) -> TranslateResult {
        debug!("ldtr");

        self.consume();
        self.expect_register().and_then(|c: char| -> TranslateResult {
            match c {
                '0'...'9' | 'a'...'f' | 'A'...'F' => {
                    self.__layout.add(N('F'));
                    self.__layout.add(N(c));
                    self.__layout.add(N('0'));
                    self.__layout.add(N('7'));

                    Ok((()))
                },
                _ => Err(format!("Only registers V0 - VF are allowed for this command; Found: {}", c))
            }
        })
    }

    fn ldkr(&mut self) -> TranslateResult {
        debug!("ldkr");

        self.consume();
        self.expect_register().and_then(|c: char| -> TranslateResult {
            match c {
                '0'...'9' | 'a'...'f' | 'A'...'F' => {
                    self.__layout.add(N('F'));
                    self.__layout.add(N(c));
                    self.__layout.add(N('0'));
                    self.__layout.add(N('A'));

                    Ok((()))
                },
                _ => Err(format!("Only registers V0 - VF are allowed for this command; Found: {}", c))
            }
        })
    }

    fn ldrt(&mut self) -> TranslateResult {
        debug!("ldrt");

        self.consume();
        self.expect_register().and_then(|c: char| -> TranslateResult {
            match c {
                '0'...'9' | 'a'...'f' | 'A'...'F' => {
                    self.__layout.add(N('F'));
                    self.__layout.add(N(c));
                    self.__layout.add(N('1'));
                    self.__layout.add(N('5'));

                    Ok((()))
                },
                _ => Err(format!("Only registers V0 - VF are allowed for this command; Found: {}", c))
            }
        })
    }

    fn ldrs(&mut self) -> TranslateResult {
        debug!("ldrs");

        self.consume();
        self.expect_register().and_then(|c: char| -> TranslateResult {
            match c {
                '0'...'9' | 'a'...'f' | 'A'...'F' => {
                    self.__layout.add(N('F'));
                    self.__layout.add(N(c));
                    self.__layout.add(N('1'));
                    self.__layout.add(N('8'));

                    Ok((()))
                },
                _ => Err(format!("Only registers V0 - VF are allowed for this command; Found: {}", c))
            }
        })
    }

    fn ldar(&mut self) -> TranslateResult {
        debug!("ldar");

        self.consume();
        self.expect_register().and_then(|c: char| -> TranslateResult {
            match c {
                '0'...'9' | 'a'...'f' | 'A'...'F' => {
                    self.__layout.add(N('F'));
                    self.__layout.add(N(c));
                    self.__layout.add(N('5'));
                    self.__layout.add(N('5'));

                    Ok((()))
                },
                _ => Err(format!("Only registers V0 - VF are allowed for this command; Found: {}", c))
            }
        })
    }

    fn ldra(&mut self) -> TranslateResult {
        debug!("ldra");

        self.consume();
        self.expect_register().and_then(|c: char| -> TranslateResult {
            match c {
                '0'...'9' | 'a'...'f' | 'A'...'F' => {
                    self.__layout.add(N('F'));
                    self.__layout.add(N(c));
                    self.__layout.add(N('6'));
                    self.__layout.add(N('5'));

                    Ok((()))
                },
                _ => Err(format!("Only registers V0 - VF are allowed for this command; Found: {}", c))
            }
        })
    }

    fn drw(&mut self) -> TranslateResult {
        println!("drw");

        self.consume();

        self.expect_register().and_then(|r1: char| -> Result<(char, char), String> {
            self.expect_comma().and_then(|()| -> Result<(char, char), String> {
                match self.expect_register() {
                    Ok(c) => Ok((r1, c)),
                    Err(e) => Err(e)
                }
            })
        }).and_then(|r: (char, char)| -> TranslateResult {
            self.__layout.add(N('D'));
            self.__layout.add(N(r.0));
            self.__layout.add(N(r.1));

            self.expect_comma().and_then(|()| -> TranslateResult {
                match self.consume() {
                    Some(Number { value: n, ..} ) => {
                        let hx = format!("{:01X}", n);
                        if hx.len() > 1 {
                            Err("Number too big".to_string())
                        } else {
                            self.__layout.add(N((&hx).chars().next().unwrap()));
                            Ok((()))
                        }
                    },
                    c @ _ => Err(format!("Expected: Number; Found: {:?}", c))
                }
            })
        })
    }

    fn expect_identifier(&mut self) -> Result<String, String> {
        debug!("Expecting identifier");
        if let Some(ref x) = self.peek() {
            match x {
                &Identifier {value: ref s, ..} => { self.consume(); Ok(s.clone()) },
                _ => Err(format!("Expected identifier, found: {:?}", x))
            }
        } else {
            Err(format!("Expected identifier, found nothing"))
        }
    }

    fn expect_register(&mut self) -> Result<char, String> {
        debug!("Expecting register");
        match self.peek() {
            Some(ref x) => match x {
                &Register {value: ref s, ..} => {
                    let mut it = (&s).chars();

                    match it.next().unwrap() {
                        'v' | 'V' => match it.next().unwrap() {
                            c @ '0'...'9' | c @ 'a'...'f' | c @ 'A'...'F'  => { 
                                debug!("Found register V{}", c);
                                self.consume();
                                Ok(c) },
                            c @ _ => {
                                debug!("Invalid register V{}", s);
                                Err(format!("Unknown register: {}", s)) }
                        },
                        'i' | 'I' => {
                            debug!("Found register I");
                            self.consume();
                            Ok('i') },
                        c @ _ => {
                            debug!("Invalid register {}", c);
                            Err(format!("Unknown register: {}", c)) }
                    }
                },
                c @ _ => {
                    debug!("Found {:?} instead of register", c);
                    Err(format!("Expected: register; Found: {:?}", c)) }
            },
            _ => Err("Expected: register; Found: EOF".to_string())
        }
    }

    fn call(&mut self) -> TranslateResult {
        self.consume();
        self.expect_label().and_then(|s: String| {
            self.__layout.add(N('2'));
            self.__layout.add(MemRef(s.clone()));
            Ok(())
        })
    }

    fn expect_label(&mut self) -> Result<String, String> {
        debug!("Expecting label");
        match self.peek() {
            Some(Identifier {value: ref s, ..}) => { self.consume(); Ok(s.clone()) },
            Some(c @ _) => Err(format!("Expected Identifier, found: {:?}", c)),
            None => Err("Expected Identifier, found: EOF".to_string())
        }
    }

    fn ret(&mut self) -> TranslateResult {
        self.consume();
        self.__layout.add(N('0'));
        self.__layout.add(N('0'));
        self.__layout.add(N('E'));
        self.__layout.add(N('E'));
        Ok(())
    }

    fn expect_number(&mut self) -> Result<u8, String> {
        debug!("Expecting number");
        match self.peek() {
            Some(Number {value: n, ..}) => { 
                debug!("Found number '{}'", n);
                self.consume();
                Ok(n) },
            Some(Error {value: s, ..}) => { 
                debug!("Found error '{}'", s);
                Err(s) },
            Some(c @ _) => {
                debug!("Found {:?} instead of number", c);
                Err(format!("Expected Number, found: {:?}", c))},
            None => {
                debug!("Found EOF instead of number");
                Err("Expected Number, found: EOF".to_string())}
        }
    }

    fn write_byte(&mut self, n: u8) -> TranslateResult {
        let s = format!("{:02X}", n);
        match s.len() {
            2 => {
                let mut it = s[..].chars();

                self.__layout.add(N(it.next().unwrap()));
                self.__layout.add(N(it.next().unwrap()));
                Ok(())
            },
            _ => Err(format!("Number {:?} is too large to be represented as a byte", n))
        }
    }

    fn se_or_sne(&mut self, se: bool) -> TranslateResult {
        self.consume();

        debug!("Parsing {}", if se { "SE" } else { "SNE" });

        self.expect_register().and_then(|c: char| -> TranslateResult {
            match c {
                c @ 'I' => Err(format!("Register '{:?}' not supported by this op", c)),
                _ => {
                    self.expect_comma().and_then(|()| -> TranslateResult {
                        self.expect_number().and_then(|n: u8| -> TranslateResult {
                            self.__layout.add(N(match se {
                                true => '3',
                                false => '4'
                            }));
                            self.__layout.add(N(c));
                            self.write_byte(n)
                        }).or_else(|err: String| -> TranslateResult {
                            if err == "Number does not fit in one byte" {
                                Err(err)
                            } else {
                                self.expect_register().and_then(|c2: char| -> TranslateResult {
                                    self.__layout.add(N(match se {
                                        true => '5',
                                        false => '9'
                                    }));
                                    self.__layout.add(N(c));
                                    self.__layout.add(N(c2));
                                    self.__layout.add(N('0'));
                                    Ok(())
                                })
                            }
                        })
                    })
                },
            }
        })
    }

    fn add(&mut self) -> TranslateResult {
        debug!("add");
        self.consume();
        self.expect_register().and_then(|r1: char| -> TranslateResult {
            self.expect_comma().and_then(|()| -> TranslateResult {
                self.expect_number().and_then(|n: u8| -> TranslateResult {
                    debug!("Branch 'add <reg> <byte>'");
                    match r1 {
                        '0'...'9' | 'a'...'f' | 'A'...'F' => {
                            self.__layout.add(N('7'));
                            self.__layout.add(N(r1));
                            self.write_byte(n) },
                        _ => Err(format!("Register '{}' not supported for this operation", r1)) 
                    }
                }).or_else(|err: String| -> TranslateResult {
                    if !err.contains("Expected Number, found") {
                        return Err(err);
                    }

                    self.expect_register().and_then(|r2: char| -> TranslateResult {
                        debug!("Branch 'add <reg> <reg>'");
                        match r1 {
                            '0'...'9' | 'a'...'f' | 'A'...'F' => {
                                debug!("Branch 'add <v-reg> <reg>'");
                                self.__layout.add(N('8'));
                                self.__layout.add(N(r1));
                                self.__layout.add(N(r2));
                                self.__layout.add(N('4'));
                                Ok(()) },
                            'i' | 'I' => {
                                debug!("Branch 'add <I-reg> <reg>'");
                                self.__layout.add(N('F'));
                                self.__layout.add(N(r2));
                                self.__layout.add(N('1'));
                                self.__layout.add(N('E'));
                                Ok(()) },
                            _ => Err(format!("Register '{}' not supported for this operation", r1))
                        }
                    })
                })
            })
        })
    }

    fn sub(&mut self) -> TranslateResult {
        debug!("sub");
        self.consume();

        self.expect_register().and_then(|r1: char| -> TranslateResult {
            self.expect_comma().and_then(|()| -> TranslateResult {
                self.expect_register().and_then(|r2: char| -> TranslateResult {
                    match r1 {
                        '0'...'9'|'a'...'f'|'A'...'F' => {
                            match r2 {
                                '0'...'9'|'a'...'f'|'A'...'F' => {
                                    self.__layout.add(N('8'));
                                    self.__layout.add(N(r1));
                                    self.__layout.add(N(r2));
                                    self.__layout.add(N('5'));

                                    Ok(()) },
                                _ => Err(format!("Register {} is not supported by this operation", r2))
                            }
                        },
                        _ => Err(format!("Register {} is not supported by this operation", r1))
                    }
                })
            })
        })
    }
    
    fn subn(&mut self) -> TranslateResult {
        debug!("subn");
        self.consume();

        self.expect_register().and_then(|r1: char| -> TranslateResult {
            self.expect_comma().and_then(|()| -> TranslateResult {
                self.expect_register().and_then(|r2: char| -> TranslateResult {
                    match r1 {
                        '0'...'9'|'a'...'f'|'A'...'F' => {
                            match r2 {
                                '0'...'9'|'a'...'f'|'A'...'F' => {
                                    self.__layout.add(N('8'));
                                    self.__layout.add(N(r1));
                                    self.__layout.add(N(r2));
                                    self.__layout.add(N('6'));

                                    Ok(()) },
                                _ => Err(format!("Register {} is not supported by this operation", r2))
                            }
                        },
                        _ => Err(format!("Register {} is not supported by this operation", r1))
                    }
                })
            })
        })
    }

    fn or(&mut self) -> TranslateResult {
        debug!("or");
        self.consume();

        self.expect_register().and_then(|r1: char| -> TranslateResult {
            self.expect_comma().and_then(|()| -> TranslateResult {
                self.expect_register().and_then(|r2: char| -> TranslateResult {
                    match r1 {
                        '0'...'9'|'a'...'f'|'A'...'F' => {
                            match r2 {
                                '0'...'9'|'a'...'f'|'A'...'F' => {
                                    self.__layout.add(N('8'));
                                    self.__layout.add(N(r1));
                                    self.__layout.add(N(r2));
                                    self.__layout.add(N('1'));

                                    Ok(()) },
                                _ => Err(format!("Register {} is not supported by this operation", r2))
                            }
                        },
                        _ => Err(format!("Register {} is not supported by this operation", r1))
                    }
                })
            })
        })
    }

    fn and(&mut self) -> TranslateResult {
        debug!("and");
        self.consume();

        self.expect_register().and_then(|r1: char| -> TranslateResult {
            self.expect_comma().and_then(|()| -> TranslateResult {
                self.expect_register().and_then(|r2: char| -> TranslateResult {
                    match r1 {
                        '0'...'9'|'a'...'f'|'A'...'F' => {
                            match r2 {
                                '0'...'9'|'a'...'f'|'A'...'F' => {
                                    self.__layout.add(N('8'));
                                    self.__layout.add(N(r1));
                                    self.__layout.add(N(r2));
                                    self.__layout.add(N('2'));

                                    Ok(()) },
                                _ => Err(format!("Register {} is not supported by this operation", r2))
                            }
                        },
                        _ => Err(format!("Register {} is not supported by this operation", r1))
                    }
                })
            })
        })
    }

    fn xor(&mut self) -> TranslateResult {
        debug!("xor");
        self.consume();

        self.expect_register().and_then(|r1: char| -> TranslateResult {
            self.expect_comma().and_then(|()| -> TranslateResult {
                self.expect_register().and_then(|r2: char| -> TranslateResult {
                    match r1 {
                        '0'...'9'|'a'...'f'|'A'...'F' => {
                            match r2 {
                                '0'...'9'|'a'...'f'|'A'...'F' => {
                                    self.__layout.add(N('8'));
                                    self.__layout.add(N(r1));
                                    self.__layout.add(N(r2));
                                    self.__layout.add(N('3'));

                                    Ok(()) },
                                _ => Err(format!("Register {} is not supported by this operation", r2))
                            }
                        },
                        _ => Err(format!("Register {} is not supported by this operation", r1))
                    }
                })
            })
        })
    }

    fn shr(&mut self) -> TranslateResult {
        debug!("shr");
        self.consume();

        self.expect_register().and_then(|r1: char| -> TranslateResult {
            let mut r2 = r1;
            let ret = self.expect_eol().and_then(|()| -> TranslateResult {
                match r1 {
                    '0'...'9'|'a'...'f'|'A'...'F' => Ok(()),
                    _ => Err(format!("Register {} is not supported by this operation", r2))
                }
            }).or_else(|_| -> TranslateResult {
                self.expect_comma().and_then(|_| -> TranslateResult {
                    self.expect_register().and_then(|reg2| -> TranslateResult {
                        match reg2 {
                            '0'...'9'|'a'...'f'|'A'...'F' => { r2 = reg2; Ok(()) },
                            _ => Err(format!("Register {} is not supported by this operation", r2))
                        }
                    })
                })
            });

            if ret.is_ok() {
                self.__layout.add(N('8'));
                self.__layout.add(N(r1));
                self.__layout.add(N(r2));
                self.__layout.add(N('6'));

                Ok(())
            } else {
                ret
            }
        })
    }

    fn shl(&mut self) -> TranslateResult {
        debug!("shl");
        self.consume();

        self.expect_register().and_then(|r1: char| -> TranslateResult {
            let mut r2 = r1;
            let ret = self.expect_eol().and_then(|()| -> TranslateResult {
                match r1 {
                    '0'...'9'|'a'...'f'|'A'...'F' => Ok(()),
                    _ => Err(format!("Register {} is not supported by this operation", r2))
                }
            }).or_else(|_| -> TranslateResult {
                self.expect_comma().and_then(|_| -> TranslateResult {
                    self.expect_register().and_then(|reg2| -> TranslateResult {
                        match reg2 {
                            '0'...'9'|'a'...'f'|'A'...'F' => { r2 = reg2; Ok(()) },
                            _ => Err(format!("Register {} is not supported by this operation", r2))
                        }
                    })
                })
            });

            if ret.is_ok() {
                self.__layout.add(N('8'));
                self.__layout.add(N(r1));
                self.__layout.add(N(r2));
                self.__layout.add(N('E'));

                Ok(())
            } else {
                ret
            }
        })
    }

    fn rnd(&mut self) -> TranslateResult {
        debug!("rnd");
        self.consume();
        self.expect_register().and_then(|r1| -> TranslateResult {
            match r1 {
                '0'...'9' | 'a'...'f' | 'A'...'F' => self.expect_comma().and_then(|_| -> TranslateResult {
                    self.expect_number().and_then(|n| -> TranslateResult {
                        self.__layout.add(N('C'));
                        self.__layout.add(N(r1));
                        self.write_byte(n)
                    })
                }),
                _ => Err(format!("Register '{}' is not supported by this operation", r1))
            }
        })
    }

    fn skp_or_sknp(&mut self, skp: bool) -> TranslateResult {
        debug!("{}", if skp { "skp" } else { "skpn" });
        self.consume();

        self.expect_register().and_then(|r| -> TranslateResult {
            match r {
                '0'...'9' | 'a'...'f' | 'A'...'F' => {
                    self.__layout.add(N('E'));
                    self.__layout.add(N(r));

                    if skp {
                        self.__layout.add(N('9'));
                        self.__layout.add(N('E'));
                    } else {
                        self.__layout.add(N('A'));
                        self.__layout.add(N('1'));
                    }

                    Ok(())
                },
                _ => Err(format!("Register '{}' is not supported by this operation", r))
            }
        })
    }
}

#[cfg(test)]
mod test {
    extern crate simple_logger;
    use std::sync::{Once, ONCE_INIT};
    use tempfile::NamedTempFile;
    use std::io::Write;

    use asm::layout::Layout;
    use asm::translator::{Translator, TranslateResult};

    static INIT: Once = ONCE_INIT;

    struct StringTranslator {
        translator: Translator,
        file: NamedTempFile
    }

    impl StringTranslator {
        fn new(value: &str) -> StringTranslator {
            let mut f = NamedTempFile::new().unwrap();
            f.write(value.as_bytes());

            StringTranslator {
                translator: Translator::from_file(f.path().to_str().unwrap()),
                file: f
            }
        }

        fn translate(&mut self) -> TranslateResult {
            self.translator.translate()
        }

        fn get_layout(&self) -> &Layout {
            self.translator.get_layout()
        }

        fn get(&self) -> String {
            match self.translator.get_layout().peek_last_word() {
                Some(s) => s,
                None => { println!("Layout is empty - could not get last word"); assert!(false); format!("") }
            }
        }
    }

    fn translation_eq(input: &str, output: &str) {
        INIT.call_once(|| {
            simple_logger::init().unwrap();
        });

        debug!("");
        debug!("--- translation_eq ( \"{}\", \"{}\" )", input, output);

        let mut s = StringTranslator::new(input);

        match s.translate() {
            Err(s) => { println!("ERROR: {}", s); assert!(false); },
            _ => ()
        }

        assert_eq!(s.get(), output);
        debug!("--- PASS");
    }

    fn translation_fail(input: &str, expected_error: &str) {
        INIT.call_once(|| {
            simple_logger::init().unwrap();
        });

        debug!("");
        debug!("--- translation_fail ( \"{}\", \"{}\" )", input, expected_error);

        let mut s = StringTranslator::new(input);
        match s.translate() {
            Err(e) => assert_eq!(e, expected_error),
            _ => { println!("Error \"{}\" not encountered!", expected_error); assert!(false); }
        };

        debug!("--- PASS");
    }

    #[test]
    fn test_number_size_check() {
        translation_fail("se $v1, 700", "Number does not fit in one byte");
        translation_fail("se $v1, 0x2BC", "Number does not fit in one byte");
        translation_fail("se $v1, 0b1010111100", "Number does not fit in one byte");
    }

    #[test]
    fn test_op_cls() {
        translation_eq("cls", "00E0");
    }

    #[test]
    fn test_op_ret() {
        translation_eq("ret", "00EE");
    }

    #[test]
    fn test_op_jp() {
        // JP addr
        // 200 is the starting address of a CHIP-8 program
        translation_eq("test_label: jp test_label", "1200");

        // JP V0, addr
        translation_eq("test_label: jp $v0, test_label", "B200");
        translation_fail("test_label: jp $v1, test_label", "This command only supports register V0. Encountered: V1");
    }

    #[test]
    fn test_op_call() {
        // 200 is the starting address of a CHIP-8 program
        translation_eq("test_label: call test_label", "2200");
    }

    #[test]
    fn test_op_se() {
        translation_eq("se $v1, 240", "31F0");
        translation_eq("se $va, 240", "3AF0");
        translation_eq("se $v2, $v5", "5250");
    }

    #[test]
    fn test_op_sne() {
        translation_eq("sne $v1, 240", "41F0");
        translation_eq("sne $v1, $v6", "9160");
    }

    #[test]
    fn test_op_ld_vx_i() {
        translation_eq("ld $vA, 235", "6AEB");
        translation_eq("test_label: ld $I, test_label", "A200");
    }

    #[test]
    fn test_op_ld_vx_vy() {
        translation_eq("ld $v1, $v5", "8150");
    }
    
    #[test]
    fn test_op_drw() {
        translation_eq("drw $vF, $v2, 10", "DF2A");
    }

    #[test]
    fn test_op_ldri() {
        translation_eq("ldri $v1", "F129");
    }

    #[test]
    fn test_op_ldtr() {
        translation_eq("ldtr $v5", "F507");
    }

    #[test]
    fn test_op_ldkr() {
        translation_eq("ldkr $v4", "F40A");
    }

    #[test]
    fn test_op_ldrt() {
        translation_eq("ldrt $v6", "F615");
    }

    #[test]
    fn test_op_ldrs() {
        translation_eq("ldrs $vC", "FC18");
    }

    #[test]
    fn test_op_bcd() {
        translation_eq("bcd $V7", "F733");
    }

    #[test]
    fn test_op_ldar() {
        translation_eq("ldar $vF", "FF55");
    }

    #[test]
    fn test_op_ldra() {
        translation_eq("ldra $vA", "FA65");
    }

    #[test]
    fn test_op_add() {
        translation_eq("add $v3, 230", "73E6");
        translation_eq("add $v3, $v9", "8394");
        translation_eq("add $I, $vF", "FF1E");
        translation_fail("add $I, 230", "Register 'i' not supported for this operation");
    }

    #[test]
    fn test_op_sub() {
        translation_eq("sub $v3, $v8", "8385");
    }

    #[test]
    fn test_op_subn() {
        translation_eq("subn $v9, $vc", "89C6");
    }

    #[test]
    fn test_op_or() {
        translation_eq("or $vF, $v5", "8F51");
    }

    #[test]
    fn test_op_and() {
        translation_eq("and $v9, $v5", "8952");
    }

    #[test]
    fn test_op_xor() {
        translation_eq("xor $v9, $vA", "89A3");
    }

    #[test]
    fn test_op_shr() {
        translation_eq("shr $v3", "8336");
        translation_eq("shr $v5, $v4", "8546");
    }

    #[test]
    fn test_op_shl() {
        translation_eq("shl $v3", "833E");
        translation_eq("shl $v5, $v4", "854E");
    }

    #[test]
    fn test_op_rnd() {
        translation_eq("rnd $v5, 235", "C5EB");
    }

    #[test]
    fn test_op_skp() {
        translation_eq("skp $v6", "E69E");
    }

    #[test]
    fn test_op_sknp() {
        translation_eq("sknp $v6", "E6A1");
    }
}

