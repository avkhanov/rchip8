#[macro_use]
extern crate glium;

mod emu;
mod common;

use std::sync::{Arc, Mutex};

use emu::mem::RChip8Memory;
use emu::gui::RChip8GLGUI;
use emu::framebuffer::RChip8DisplayBuffer;
use emu::input::RChip8Input;
use emu::cpu::RChip8CPU;


fn main() {
    let mut args = std::env::args();

    if args.len() != 2 {
        println!("Usage: rchip8 <ROM-FILE-PATH>");
        return;
    }

    let disp_buffer = Arc::new(Mutex::new(RChip8DisplayBuffer::new()));
    let memory = Arc::new(Mutex::new(RChip8Memory::new(
        &(args.nth(1).unwrap())
    )));
    let input = Arc::new(Mutex::new(RChip8Input::new()));

    let mut gui = RChip8GLGUI::new(disp_buffer.clone(), input.clone());
    let mut processor = RChip8CPU::new(disp_buffer.clone(), memory.clone(), input.clone());

    gui.start();
    processor.start();
}

